﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace XMLToJSON
{
    class Program
    {
        static void Main(string[] args)
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string path = Path.GetFullPath(Path.Combine(currentDirectory, @"../../../../"));

            Console.WriteLine("Current directory is " + path);
            Console.WriteLine("Welcome to XML to JSON converter. Please supply the name of the XML file.");

            var filename = Console.ReadLine();
            XElement ax= LoadFile(path, filename);
            string json = ConvertXml(ax);

            SaveToFile(filename, json, path);
        }

        private static XElement LoadFile(string path, string filename)
        {

            if(!filename.Contains(".xml", StringComparison.InvariantCulture))
            {
                filename += ".xml";
            }
            try
            {
                XElement ax = XElement.Load(path + filename);
                if(ax != null)
                {
                    Console.WriteLine("File succesfully loaded!");
                    return ax;
                }
                throw new Exception("The file is empty.");
            }
            catch (Exception e )
            {
                Console.WriteLine("The file {0} could not be loaded.", filename);
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private static string ConvertXml(XElement ax)
        {
            XmlDocument doc = new XmlDocument();

            if(ax == null)
            {
                return "";
            }

            doc.LoadXml(ax.ToString());
            try
            {
                string json = JsonConvert.SerializeXmlNode(doc);
                Console.WriteLine("XML converted successfully");
                return json;
            }
            catch(Exception e)
            {
                Console.WriteLine("Could not convert XML file to JSON. Exception: " + e.Message);
                return null;
            }
        }

        private static void SaveToFile(string filename, string json, string path)
        {
            
            path +=  filename +".json";
            System.IO.File.WriteAllText(path, json);
            Console.WriteLine("JSON file saved {0}", path);

        }
    }
}
