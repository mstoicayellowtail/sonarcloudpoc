FROM dotnet-core-sdk-2.2-debug:latest AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY XMLToJSON/XMLToJSON.csproj XMLToJSON/

RUN dotnet restore XMLToJSON/AccessIdXMLToJSONentityAPI.csproj
RUN dotnet list XMLToJSON/XMLToJSON.csproj package

# Copy everything else and build
COPY ./ ./
RUN dotnet publish XMLToJSON/XMLToJSON.csproj -c Debug -o out --no-restore

# Build runtime image
FROM dotnet-core-aspnet-2.1-debug:latest
WORKDIR /app
COPY --from=build-env /app/XMLToJSON/out .
ENTRYPOINT ["dotnet", "XMLToJSON.dll"]

